<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoneyTrade extends Model
{
    //  /**
    //  * The attributes that are mass assignable.
    //  *
    //  * @var array
    //  */
    // protected $fillable = [
    //     'bank', 'email', 'mt_first_name', 'mt_last_name', 'mt_password', 'mt_account', 'mt_deposit', 'mt_leverage',
    // ];

    // /**
    //  * The attributes that should be hidden for arrays.
    //  *
    //  * @var array
    //  */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    // /**
    //  * The attributes that should be cast to native types.
    //  *
    //  * @var array
    //  */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

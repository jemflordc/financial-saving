<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('register/agreement', 'Auth\RegisterController@show')->name('register.agreement');
// Route::get('register/moneyTrade/{moneyTrade}', 'MoneyTradeController@store')->name('moneyTrade.store')->middleware('auth');
// Route::get('register/moneytrade/show/{moneyTrade}', 'MoneyTradeController@show')->name('register.moneytrade.show');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('moneytrade', 'MoneyTradeController')->middleware('auth');
Route::get('/moneytrade', 'MoneyTradeController@store')->middleware('auth');
Route::get('/moneytrade/registration', 'MoneyTradeController@index')->name('mt.registration')->middleware('auth');

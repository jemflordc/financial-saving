<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoneyTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('money_trades', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('mt_number');
            $table->unsignedBigInteger('user_id');
            $table->string('bank');
            $table->string('email');
            $table->string('mt_first_name');
            $table->string('mt_last_name');
            $table->string('mt_password');
            $table->string('mt_account');
            $table->string('mt_deposit');
            $table->string('mt_leverage');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('money_trades');
    }
}

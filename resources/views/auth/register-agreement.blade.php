@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">


            <form method="POST" action="{{ route('register') }}">
                @csrf

            <div class="card">
                <div class="card-header">{{ __('REGISTRATION') }}</div>

                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-md-8">

                            <div class="card">
                                <div class="card-header">{{ __('Policy Owner Information') }}</div>

                                <div class="card-body">

                                    <div class="form-group row">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email should be valid and active" required autocomplete="email">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                                        <div class="col-md-6">
                                            <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="Valid username max of 15 characters" required autocomplete="username" autofocus>

                                            @error('username')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                                        <div class="col-md-6">
                                            <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" placeholder="First name of the policy owner" required autocomplete="first_name" autofocus>

                                            @error('first_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                                        <div class="col-md-6">
                                            <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" placeholder="Last name of the policy owner" required autocomplete="last_name" autofocus>

                                            @error('last_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="dob" class="col-md-4 col-form-label text-md-right">{{ __('Date of Birth') }}</label>

                                        <div class="col-md-6">
                                            <input id="dob" type="date" class="form-control @error('dob') is-invalid @enderror" name="dob" value="{{ old('dob') }}" required autocomplete="dob" autofocus>

                                            @error('dob')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="landline" class="col-md-4 col-form-label text-md-right">{{ __('Landline Number') }}</label>

                                        <div class="col-md-6">
                                            <input id="landline" type="number" class="form-control @error('landline') is-invalid @enderror" name="landline" value="{{ old('landline') }}" required autocomplete="landline" placeholder="(area code)(telephone number)" autofocus>

                                            @error('landline')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="mobile" class="col-md-4 col-form-label text-md-right">Mobile Number <span>+63</span></label>

                                        <div class="col-md-6">
                                            <input id="mobile" type="number" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile" placeholder="9123456789" autofocus>

                                            @error('mobile')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Minimum of 8 characters" required autocomplete="new-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Minimum of 8 characters" required autocomplete="new-password">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>

                            <div class="card">
                                <div class="card-header">{{ __('Security Question') }}</div>

                                <div class="card-body">

                                    <div class="form-group row">
                                        <label for="question1" class="col-md-4 col-form-label text-md-right">{{ __('Question #1') }}</label>

                                        <div class="col-md-6">

                                            <select name="question1" class="form-control @error('question1') is-invalid @enderror">
                                                <option value="q1">What is your favorite color?</option>
                                                <option value="q2">What city were you born?</option>
                                                <option value="q3">What is your favorite food?</option>
                                            </select>
                                            {{-- <input id="question1" type="question1" class="form-control @error('question1') is-invalid @enderror" name="question1" value="{{ old('question1') }}" placeholder="Email should be valid and active" required autocomplete="question1"> --}}

                                            @error('question1')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="question1_answer" class="col-md-4 col-form-label text-md-right">{{ __('Answer') }}</label>

                                        <div class="col-md-6">

                                            <input id="question1_answer" type="password" class="form-control @error('question1_answer') is-invalid @enderror" name="question1_answer" value="{{ old('question1_answer') }}" placeholder="Email should be valid and active" required autocomplete="question1_answer">

                                            @error('question1_answer')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="question2" class="col-md-4 col-form-label text-md-right">{{ __('Question #2') }}</label>

                                        <div class="col-md-6">

                                            <select name="question2" class="form-control @error('question2') is-invalid @enderror">
                                                <option value="q1">What is your favorite color?</option>
                                                <option value="q2">What city were you born?</option>
                                                <option value="q3">What is your favorite food?</option>
                                            </select>
                                            {{-- <input id="question2" type="question2" class="form-control @error('question2') is-invalid @enderror" name="question2" value="{{ old('question2') }}" placeholder="Email should be valid and active" required autocomplete="question2"> --}}

                                            @error('question2')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="question2_answer" class="col-md-4 col-form-label text-md-right">{{ __('Answer') }}</label>

                                        <div class="col-md-6">

                                            <input id="question2_answer" type="password" class="form-control @error('question2_answer') is-invalid @enderror" name="question2_answer" value="{{ old('question2_answer') }}" placeholder="Email should be valid and active" required autocomplete="question2_answer">

                                            @error('question2_answer')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>

                            {{-- <div class="card">
                                <div class="card-header">{{ __('Policy Information') }}</div>

                                <div class="card-body">

                                    <div class="form-group row">
                                        <label for="plan" class="col-md-4 col-form-label text-md-right">{{ __('Policy Plan') }}</label>

                                        <div class="col-md-6">
                                            <input id="plan" type="plan" class="form-control @error('plan') is-invalid @enderror" name="plan" value="{{ old('plan') }}" placeholder="Select Policy Plan" required autocomplete="plan">

                                            @error('plan')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="policy_date" class="col-md-4 col-form-label text-md-right">{{ __('Policy Effectivity Date') }}</label>

                                        <div class="col-md-6">
                                            <input id="policy_date" type="date" class="form-control @error('policy_date') is-invalid @enderror" name="policy_date" value="{{ old('policy_date') }}" required autocomplete="policy_date" autofocus>

                                            @error('policy_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
            </div>
            </form>

        </div>
    </div>
</div>
@endsection

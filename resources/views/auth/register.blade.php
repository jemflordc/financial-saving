@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form>


                        <h2>AGREEMENT</h2>

                            <p>Agreement text here</p>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                  <label class="form-check-label"><span class="required">*</span>
                                    <input type="checkbox" class="form-check-input" name="agree" id="" value="checkedValue" checked>
                                    I Agree
                                  </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="checkbox" class="form-check-input" name="agree" id="" value="checkedValue" disabled>
                                      I Disagree
                                    </label>
                                  </div>
                                <button type="submit" class="btn btn-primary">
                                <a href="{{ route('register.agreement')}}">Enroll now</a>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

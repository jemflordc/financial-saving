@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}


                    <div class="container">
                        <!-- Trigger the modal with a button -->
                        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#moneytradeModal">Connect to an Account</button>
                    <button type="button" class="btn btn-info btn-lg"><a href="{{route('mt.registration')}}">Registration</a> </button>

                        <!-- Moneytrade Modal -->
                        <div class="modal fade" id="moneytradeModal" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Connect to an Account</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              <div class="modal-body">
                                <p><center> Authorization allows to get access to your Money account </center></p>

                                <div class="form-group row">
                                    <label for="mt_email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="mt_email" type="mt_email" class="form-control @error('mt_email') is-invalid @enderror" name="mt_email" value="{{ old('mt_email') }}" required autocomplete="mt_email" autofocus>

                                        @error('mt_email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="mt_password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="mt_password" type="mt_password" class="form-control @error('mt_password') is-invalid @enderror" name="mt_password" required autocomplete="current-password">

                                        @error('mt_password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="bank" class="col-md-4 col-form-label text-md-right">{{ __('Bank') }}</label>

                                    <div class="col-md-6">
                                        <input id="bank" type="text" class="form-control @error('bank') is-invalid @enderror" name="bank" value="MoneyTrade Bank" required autocomplete="current-bank" readonly>

                                        @error('bank')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#registerModal">Register</button>
                                <button type="button" class="btn btn-default">Login</button>
                                {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --}}
                              </div>
                            </div>

                          </div>
                        </div>

                        <!-- Moneytrade Register Modal -->
                        <div class="modal fade" id="registerModal" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Open an Account</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">

                                        <form action="{{route('moneytrade.store')}}" method="post">
                                            @csrf

                                            <h3><b>Personal Details</b></h3>
                                            <p><center> To open an account, please fill out the following fields:</center></p>

                                            <div class="form-group row">
                                                <label for="bank" class="col-md-4 col-form-label text-md-right">{{ __('Bank') }}</label>

                                                <div class="col-md-6">
                                                    <input id="bank" type="text" class="form-control @error('bank') is-invalid @enderror" name="bank" value="MoneyTrade Bank" required autocomplete="current-bank" readonly>

                                                    @error('bank')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                                <div class="col-md-6">
                                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="mt_first_name" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                                                <div class="col-md-6">
                                                    <input id="mt_first_name" type="text" class="form-control @error('mt_first_name') is-invalid @enderror" name="mt_first_name" value="{{ old('mt_first_name') }}" placeholder="First name of the account owner" required autocomplete="mt_first_name" autofocus>

                                                    @error('mt_first_name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="mt_last_name" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                                                <div class="col-md-6">
                                                    <input id="mt_last_name" type="text" class="form-control @error('mt_last_name') is-invalid @enderror" name="mt_last_name" value="{{ old('mt_last_name') }}" placeholder="Last name of the account owner" required autocomplete="mt_last_name" autofocus>

                                                    @error('mt_last_name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror

                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="mt_password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                                <div class="col-md-6">
                                                    <input id="mt_password" type="password" class="form-control @error('mt_password') is-invalid @enderror" name="mt_password" placeholder="Minimum of 8 characters" required autocomplete="new-mt_password">

                                                    @error('mt_password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="mt_password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                                <div class="col-md-6">
                                                    <input id="mt_password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Minimum of 8 characters" required autocomplete="new-password">
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="" checked>Use hedge in investment and trading</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="mt_account" class="col-md-4 col-form-label text-md-right">{{ __('Account Type') }}</label>

                                                <div class="col-md-6">
                                                    <select name="mt_account" class="form-control @error('mt_account') is-invalid @enderror">
                                                        <option value="forex-php">forex-php</option>
                                                        <option value="forex-usd">forex-usd</option>
                                                    </select>
                                                    @error('mt_account')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="mt_deposit" class="col-md-4 col-form-label text-md-right">{{ __('Deposit') }}</label>

                                                <div class="col-md-6">
                                                    <input id="mt_deposit" type="text" class="form-control @error('mt_deposit') is-invalid @enderror" name="mt_deposit" value="2500" placeholder="Last name of the policy owner" required autocomplete="mt_deposit" autofocus >

                                                    @error('mt_deposit')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="mt_leverage" class="col-md-4 col-form-label text-md-right">{{ __('Leverage') }}</label>

                                                <div class="col-md-6">
                                                    <select name="mt_leverage" class="form-control @error('mt_leverage') is-invalid @enderror">
                                                        <option value="l100">100</option>
                                                        <option value="l500">500</option>
                                                        <option value="l1000">1000</option>
                                                    </select>
                                                    @error('mt_leverage')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror

                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="" checked>I agree with the terms and conditions for opening an account and the data protection policy.</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-default">Next</button>


                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --}}
                                            </div>
                                        </form>
                                    </div>
                            </div>

                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection

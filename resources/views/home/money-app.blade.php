@extends('layouts.app')

@section('content')
                        <!-- Moneytrade Modal -->
                        <div class="modal fade" id="moneytradeModal" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">Connect to an Account</h4>
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                  <p><center> Authorization allows to get access to your Money account </center></p>

                                  <div class="form-group row">
                                      <label for="mt_email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                      <div class="col-md-6">
                                          <input id="mt_email" type="mt_email" class="form-control @error('mt_email') is-invalid @enderror" name="mt_email" value="{{ old('mt_email') }}" required autocomplete="mt_email" autofocus>

                                          @error('mt_email')
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $message }}</strong>
                                              </span>
                                          @enderror
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label for="mt_password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                      <div class="col-md-6">
                                          <input id="mt_password" type="mt_password" class="form-control @error('mt_password') is-invalid @enderror" name="mt_password" required autocomplete="current-password">

                                          @error('mt_password')
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $message }}</strong>
                                              </span>
                                          @enderror
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label for="bank" class="col-md-4 col-form-label text-md-right">{{ __('Bank') }}</label>

                                      <div class="col-md-6">
                                          <input id="bank" type="text" class="form-control @error('bank') is-invalid @enderror" name="bank" value="MoneyTrade Bank" required autocomplete="current-bank" readonly>

                                          @error('bank')
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $message }}</strong>
                                              </span>
                                          @enderror
                                      </div>
                                  </div>

                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#registerModal">Register</button>
                                  <button type="button" class="btn btn-default">Login</button>
                                  {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --}}
                                </div>
                              </div>

                            </div>
                          </div>
@endsection
